﻿using System.Collections.Generic;
using Soomla.Store;


public class StoreEventHandler
{
    public StoreEventHandler ()
    {
        StoreEvents.OnItemPurchased += onItemPurchased;
        StoreEvents.OnGoodEquipped += onGoodEquipped;
        StoreEvents.OnGoodUnEquipped += onGoodUnequipped;
        StoreEvents.OnGoodUpgrade += onGoodUpgrade;
        StoreEvents.OnGoodBalanceChanged += onGoodBalanceChanged;
        StoreEvents.OnBillingSupported += onBillingSupported;
        StoreEvents.OnBillingNotSupported += onBillingNotSupported;
        StoreEvents.OnMarketPurchase += onMarketPurchase;
        StoreEvents.OnMarketRefund += onMarketRefund;
        StoreEvents.OnMarketPurchaseStarted += onMarketPurchaseStarted;
        StoreEvents.OnMarketPurchaseCancelled += onMarketPurchaseCancelled;
        StoreEvents.OnUnexpectedStoreError += onUnexpectedStoreError;
        StoreEvents.OnItemPurchaseStarted += onItemPurchaseStarted;
        StoreEvents.OnCurrencyBalanceChanged += onCurrencyBalanceChanged;
        StoreEvents.OnRestoreTransactionsStarted += 
            onRestoreTransactionsStarted;
        StoreEvents.OnRestoreTransactionsFinished += 
            onRestoreTransactionsFinished;
        StoreEvents.OnSoomlaStoreInitialized += onSoomlaStoreInitialized;
    }

    public void onSoomlaStoreInitialized ()
    {
        DebugTextUI.DebugEvent.Invoke ("Store Initialized");

        string vgs_id = "VG: ";
        foreach (VirtualGood vg in StoreInfo.Goods) {
            vgs_id += vg.ItemId + ",";
        }

        string vcps_id = "\nVCP: ";
        foreach (VirtualCurrencyPack vcp in StoreInfo.CurrencyPacks) {
            vcps_id += vcp.ItemId + ",";
        }

        DebugTextUI.DebugEvent.Invoke (vgs_id + vcps_id);
    }

    public void onCurrencyBalanceChanged (VirtualCurrency vc, 
                                          int balance, 
                                          int amount)
    {
        DebugTextUI.DebugEvent.Invoke (
            "currency: " + vc.ItemId + ":" + vc.GetBalance ());
    }

    public void onGoodEquipped (EquippableVG good)
    {
    }

    public void onGoodUnequipped (EquippableVG good)
    {
    }

    public void onGoodUpgrade (VirtualGood good, UpgradeVG currentUpgrade)
    {
    }

    public void onBillingSupported ()
    {
    }

    public void onBillingNotSupported ()
    {
    }

    public void onMarketPurchase (PurchasableVirtualItem pvi, 
                                  string payload, 
                                  Dictionary<string, string> extra)
    {
        DebugTextUI.DebugEvent.Invoke (
            "market purchase " + pvi.ItemId);
    }

    public void onMarketRefund (PurchasableVirtualItem pvi)
    {
    }

    public void onMarketPurchaseStarted (PurchasableVirtualItem pvi)
    {
        DebugTextUI.DebugEvent.Invoke (
            "market purchase started");
    }

    public void onMarketPurchaseCancelled (PurchasableVirtualItem pvi)
    {
        DebugTextUI.DebugEvent.Invoke (
            "market purchase cancelled");
    }

    public void onUnexpectedStoreError (int errorCode)
    {
        DebugTextUI.DebugEvent.Invoke (
            string.Format ("error {0}", errorCode));
    }

    public void onItemPurchased (PurchasableVirtualItem pvi, string payload)
    {
        DebugTextUI.DebugEvent.Invoke ("purchased: " + pvi.ItemId);
    }

    public void onItemPurchaseStarted (PurchasableVirtualItem pvi)
    {
    }

    public void onGoodBalanceChanged (VirtualGood good, 
                                      int balance,
                                      int amountAdded)
    {
        DebugTextUI.DebugEvent.Invoke (good.ItemId + ":" + good.GetBalance ());
    }

    public void onRestoreTransactionsStarted ()
    {
    }

    public void onRestoreTransactionsFinished (bool success)
    {
    }
}