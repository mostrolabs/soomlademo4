﻿using Soomla.Store;
using UnityEngine;


public class StoreScript : MonoBehaviour
{
    public void BuyAContinue ()
    {
        try {
            StoreInventory.BuyItem ("continue_level");
        } catch (System.Exception ex) {
            Debug.Log (ex.Message);
        }
    }
}