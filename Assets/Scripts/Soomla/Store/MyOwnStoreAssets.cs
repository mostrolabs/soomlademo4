using System.Collections.Generic;
using Soomla.Store;

public class MyOwnStoreAssets : IStoreAssets
{
    public int GetVersion ()
    {
        return 0;
    }

    public VirtualCurrency[] GetCurrencies ()
    {
        return new []{ GEM_CURRENCY };
    }

    public VirtualGood[] GetGoods ()
    {
        return new [] {
            LIFE_ONE_GOOD,
            LIFE_THREE_GOOD,
            LIFE_FIVE_GOOD,
            CONTINUELEVEL_GOOD,
            NO_ADS_LTVG
        };
    }

    public VirtualCurrencyPack[] GetCurrencyPacks ()
    {
        return new [] { GEMS400_PACK, GEMS1000_PACK };
    }

    public VirtualCategory[] GetCategories ()
    {
        return new []{ GENERAL_CATEGORY };
    }

    /** Static Final Members **/
    public const string GEM_CURRENCY_ITEM_ID = "currency_gem";
    public const string GEM400_PACK_PRODUCT_ID = "gems_400";
    public const string GEM1000_PACK_PRODUCT_ID = "gems_1000";
    public const string LIFE_ITEM_ONE_ID = "life_one";
    public const string LIFE_ITEM_THREE_ID = "lives_three";
    public const string LIFE_ITEM_FIVE_ID = "lives_five";
    public const string CONTINUELEVEL_ITEM_ID = "continue_level";
    public const string NO_ADS_LIFETIME_PRODUCT_ID = "no_ads";

    /** Virtual Currencies **/
    public static VirtualCurrency GEM_CURRENCY = 
        new VirtualCurrency (
            "Gems",
            "Game currency",
            GEM_CURRENCY_ITEM_ID
        );

    /** Virtual Currency Packs **/
    public static VirtualCurrencyPack GEMS400_PACK = 
        new VirtualCurrencyPack (
            "400 gems",
            "400 gems pack",
            GEM400_PACK_PRODUCT_ID,
            400,
            GEM_CURRENCY_ITEM_ID,
            new PurchaseWithMarket (GEM400_PACK_PRODUCT_ID, 4.99)
        );

    public static VirtualCurrencyPack GEMS1000_PACK = 
        new VirtualCurrencyPack (
            "1000 Gems",
            "1000 Gems pack",
            GEM1000_PACK_PRODUCT_ID,
            1000,
            GEM_CURRENCY_ITEM_ID,
            new PurchaseWithMarket (GEM1000_PACK_PRODUCT_ID, 8.99)
        );

    /** Virtual Goods **/
    public static VirtualGood LIFE_ONE_GOOD = 
        new SingleUseVG (
            "Player Life",
            "Life to continue playing",
            LIFE_ITEM_ONE_ID,
            new PurchaseWithMarket (LIFE_ITEM_ONE_ID, 0.99)
        );

    public static VirtualGood LIFE_THREE_GOOD = 
        new SingleUseVG (
            "One Life",
            "One life.",
            LIFE_ITEM_THREE_ID,
            new PurchaseWithMarket (LIFE_ITEM_THREE_ID, 2.79)
        );

    public static VirtualGood LIFE_FIVE_GOOD = 
        new SingleUseVG (
            "Three Lives",
            "Three lives pack",
            LIFE_ITEM_FIVE_ID,
            new PurchaseWithMarket (LIFE_ITEM_FIVE_ID, 4.59)
        );

    public static VirtualGood CONTINUELEVEL_GOOD = 
        new SingleUseVG (
            "Continue Level",
            "Continue playing the same level from death point",
            CONTINUELEVEL_ITEM_ID,
            new PurchaseWithMarket ("android.test.purchased", 0.99)
        );

    /** Virtual Categories **/
    public static VirtualCategory GENERAL_CATEGORY = 
        new VirtualCategory (
            "General", new List<string> (
            new [] {
                LIFE_ITEM_ONE_ID, 
                LIFE_ITEM_THREE_ID,
                LIFE_ITEM_FIVE_ID,
                CONTINUELEVEL_ITEM_ID
            })
        );


    /** LifeTimeVGs **/
    public static VirtualGood NO_ADS_LTVG = 
        new LifetimeVG (
            "No Ads",
            "No More Ads!",
            NO_ADS_LIFETIME_PRODUCT_ID,
            new PurchaseWithMarket (NO_ADS_LIFETIME_PRODUCT_ID, 0.99)
        );
}
