﻿using Soomla;
using Soomla.Profile;
using UnityEngine;


public class ProfileScript : MonoBehaviour
{
    Reward coinsReward100 = new VirtualItemReward (
                                "currency_reward_100", "100 Gems Reward",
                                "currency_gem", 100);
    Reward coinsReward150 = new VirtualItemReward (
                                "currency_reward_150", "150 Gems Reward",
                                "currency_gem", 150);


    public void ShareStoryFB ()
    {
        SoomlaProfile.UpdateStory (
            Provider.FACEBOOK,                          // Provider
            "I'm playing Dino Jump",                    // Text of the story to post
            "Dino Jump",                                // Name
            "Play for free",                            // Caption
            "Play Dino Jump now",                       // Description
            "http://example.com/dinojump",              // Link to post
            "",  // Image URL
            "",                                         // Payload
            coinsReward100                              // Reward for posting a story
        );
    }

    public void LikeFB ()
    {
        SoomlaProfile.Like (
            Provider.FACEBOOK,                 // Provider  
            "The.Soomla.Project",              // Page id to like
            coinsReward150                     // Reward for liking
        );
    }
}
