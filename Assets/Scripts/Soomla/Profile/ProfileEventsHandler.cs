﻿using Soomla.Profile;
using UnityEngine;

//using Grow.Leaderboards;


public class ProfileEventsHandler
{
    public ProfileEventsHandler ()
    {
        ProfileEvents.OnSoomlaProfileInitialized += onSoomlaProfileInitialized;
        ProfileEvents.OnUserRatingEvent += onUserRatingEvent;
        ProfileEvents.OnUserProfileUpdated += onUserProfileUpdated;
        ProfileEvents.OnLoginStarted += onLoginStarted;
        ProfileEvents.OnLoginFinished += onLoginFinished;
        ProfileEvents.OnLoginCancelled += onLoginCancelled;
        ProfileEvents.OnLoginFailed += onLoginFailed;
        ProfileEvents.OnLogoutStarted += onLogoutStarted;
        ProfileEvents.OnLogoutFinished += onLogoutFinished;
        ProfileEvents.OnLogoutFailed += onLogoutFailed;
        ProfileEvents.OnSocialActionStarted += onSocialActionStarted;
        ProfileEvents.OnSocialActionFinished += onSocialActionFinished;
        ProfileEvents.OnSocialActionCancelled += onSocialActionCancelled;
        ProfileEvents.OnSocialActionFailed += onSocialActionFailed;
        ProfileEvents.OnGetContactsStarted += onGetContactsStarted;
        ProfileEvents.OnGetContactsFinished += onGetContactsFinished;
        ProfileEvents.OnGetContactsFailed += onGetContactsFailed;
        ProfileEvents.OnGetFeedStarted += onGetFeedStarted;
        ProfileEvents.OnGetFeedFinished += onGetFeedFinished;
        ProfileEvents.OnGetFeedFailed += onGetFeedFailed;
    }


    public void onSoomlaProfileInitialized ()
    {
        Debug.Log ("Soomla profile initialized");
    }

    public void onUserRatingEvent ()
    {
        // ... your game specific implementation here ...
    }

    public void onUserProfileUpdated (UserProfile userProfileJson)
    {
        // userProfileJson is the user's profile from the logged in provider
		
        // ... your game specific implementation here ...
    }

    public void onLoginStarted (Provider provider, bool autoLogin, string payload)
    {
        // provider is the social provider
        // payload is an identification string that you can give when you initiate the login operation and want to receive back upon starting
		
        // ... your game specific implementation here ...
    }

    public void onLoginFinished (UserProfile userProfileJson, bool autoLogin, string payload)
    {
        // Login with FB on GameSparks too
        /*if (SoomlaProfile.IsLoggedIn (Provider.FACEBOOK)) {
			new FacebookConnectRequest ().SetAccessToken (FB.AccessToken).Send ((response) =>
			{
				if (response.HasErrors) {
					Debug.Log (response.Errors.JSON);
				} else {
					Global.userID = response.UserId;
					Debug.Log ("Logged " + Global.userID);
				}
			});
		}*/
    }

    public void onLoginCancelled (Provider provider, bool autoLogin, string payload)
    {
        // provider is the social provider
        // payload is an identification string that you can give when you initiate the login operation and want to receive back upon cancellation
		
        // ... your game specific implementation here ...	
    }

    public void onLoginFailed (Provider provider, string message, bool autoLogin, string payload)
    {
        // provider is the social provider
        // message is the failure message
        // payload is an identification string that you can give when you initiate the login operation and want to receive back upon failure
		
        // ... your game specific implementation here ...
    }

    public void onLogoutStarted (Provider provider)
    {
        // provider is the social provider
		
        // ... your game specific implementation here ...
    }

    public void onLogoutFinished (Provider provider)
    {
        // provider is the social provider
		
        // ... your game specific implementation here ...
    }

    public void onLogoutFailed (Provider provider, string message)
    {
        // provider is the social provider
        // message is the failure message
		
        // ... your game specific implementation here ...
    }

    public void onSocialActionStarted (Provider provider, SocialActionType action, string payload)
    {
        // provider is the social provider
        // action is the social action (like, post status, etc..) that started
        // payload is an identification string that you can give when you initiate the social action operation and want to receive back upon starting
		
        // ... your game specific implementation here ...
    }

    public void onSocialActionFinished (Provider provider, SocialActionType action, string payload)
    {
        // provider is the social provider
        // action is the social action (like, post status, etc..) that finished
        // payload is an identification string that you can give when you initiate the social action operation and want to receive back upon its completion
		
        // ... your game specific implementation here ...
    }

    public void onSocialActionCancelled (Provider provider, SocialActionType action, string payload)
    {
        // provider is the social provider
        // action is the social action (like, post status, etc..) that has been cancelled
        // payload is an identification string that you can give when you initiate the social action operation and want to receive back upon cancellation
		
        // ... your game specific implementation here ...
    }

    public void onSocialActionFailed (Provider provider, SocialActionType action, string message, string payload)
    {
        // provider is the social provider
        // action is the social action (like, post status, etc..) that failed
        // message is the failure message
        // payload is an identification string that you can give when you initiate the social action operation and want to receive back upon failure
		
        // ... your game specific implementation here ...
    }

    public void onGetContactsStarted (Provider provider, bool fromStart, string payload)
    {
        // provider is the social provider
        // fromStart Should we reset pagination or request the next page
        // payload is an identification string that you can give when you initiate the get contacts operation and want to receive back upon starting
		
        // ... your game specific implementation here ...
    }

    public void onGetContactsFinished (Provider provider, 
                                       SocialPageData<UserProfile> userProfiles,
                                       string payload)
    {
        /*Debug.Log ("OnGetContactsFinished");

        // Extract a list of profile IDs from a list of friends
        System.Collections.Generic.List<string> profileIdList =
            userProfiles.PageData.ConvertAll (e => e.ProfileId);

        // Fetch friends' states
        GrowLeaderboards.FetchFriendsStates (provider.toInt (), profileIdList);

        if (userProfiles.HasMore) {
            SoomlaProfile.GetContacts (provider, false);
        } else {
            // no pages anymore
        }*/
    }

    public void onGetContactsFailed (Provider provider, string message, bool fromStart, string payload)
    {
        // provider is the social provider
        // message Description of the reason for failure
        // fromStart Should we reset pagination or request the next page
        // payload is an identification string that you can give when you initiate the get contacts operation and want to receive back upon failure
		
        // ... your game specific implementation here ...
    }

    public void onGetFeedStarted (Provider provider)
    {
        // provider is the social provider
        
        // ... your game specific implementation here ...
    }

    
    public void onGetFeedFinished (Provider provider, SocialPageData<string> feed)
    {
        
        // provider is the social provider
        // feed is the user's feed that has been fetched in the get feed operation
        
        // ... your game specific implementation here ...
    }

    public void onGetFeedFailed (Provider provider, string payload)
    {
        // provider is the social provider
        // payload is an identification string that you can give when you initiate the get feed operation and want to receive back upon failure
		
        // ... your game specific implementation here ...
    }

}
