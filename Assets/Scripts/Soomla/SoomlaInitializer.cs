﻿//using Grow.Highway;
//using Grow.Sync;
//using Grow.Insights;
using Soomla.Store;
using Soomla.Profile;
using Soomla.Levelup;
using UnityEngine;

public class SoomlaInitializer : MonoBehaviour
{
    void Start ()
    {
        /*#if !UNITY_EDITOR
        GrowHighway.Initialize ();
        #endif
        GrowInsights.Initialize ();

        GrowSync.Initialize (false, true); // Sync only state*/

        // Soomla Store
        new StoreEventHandler ();
        SoomlaStore.Initialize (new MyOwnStoreAssets ());

        // Soomla Profile
        new ProfileEventsHandler ();
        SoomlaProfile.Initialize ();

        // Soomla LevelUp
        new LevelUpEventsHandler ();
        World dragonWorld = new DragonXWorld ().CreateInitialWorld ();
        SoomlaLevelUp.Initialize (dragonWorld);
    }
}