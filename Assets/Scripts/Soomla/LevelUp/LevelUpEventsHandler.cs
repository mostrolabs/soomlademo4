﻿using Soomla.Levelup;
using UnityEngine;

public class LevelUpEventsHandler
{
    public LevelUpEventsHandler ()
    {
        LevelUpEvents.OnWorldCompleted += onWorldCompleted;
        LevelUpEvents.OnLevelStarted += onLevelStarted;
        LevelUpEvents.OnLevelEnded += onLevelEnded;
        LevelUpEvents.OnScoreRecordChanged += onScoreRecordChanged;
        LevelUpEvents.OnScoreRecordReached += onScoreRecordReached;
    }

    public void onWorldCompleted (World world)
    {
        Debug.Log ("World completed " + world.ID);
    }

    public void onLevelStarted (Level level)
    {
        Debug.Log ("Level started " + level.ID);
    }

    public void onLevelEnded (Level level)
    {
        if (level.IsCompleted ()) {
            Debug.Log ("Level completed " + level.ID);
        } else {
            Debug.Log ("Level ended " + level.ID);
        }
    }

    public void onScoreRecordReached (Score score)
    {
        Debug.Log ("score reached " + score.ID + " " + score.Record);
    }

    public void onScoreRecordChanged (Score score)
    {
        Debug.Log ("score changed " + score.ID + " " + score.Latest);
    }
}
