﻿using System.Collections.Generic;
using Soomla.Levelup;
using Soomla;

public class DragonXWorld
{
    public World CreateInitialWorld ()
    {
        /** Scores **/
        Score levelScore = new Score (
                               "levelScoreID",
                               "Level Score",
                               true
                           );

        /** Worlds **/
        World universe = new World ("Universe");
        World world01 = new World ("World01");
        World world02 = new World ("World02");
        World world03 = new World ("World03");
        World world04 = new World ("World04");

        /** Levels **/
        world01.BatchAddLevelsWithTemplates (
            12,
            null,
            new List<Score> (){ levelScore },
            null
        );

        world02.BatchAddLevelsWithTemplates (
            12,
            null,
            new List<Score> (){ levelScore },
            null
        );

        world03.BatchAddLevelsWithTemplates (
            12,
            null,
            new List<Score> (){ levelScore },
            null
        );

        world04.BatchAddLevelsWithTemplates (
            12,
            null,
            new List<Score> (){ levelScore },
            null
        );

        /** Add Worlds to Initial World **/
        universe.InnerWorldsMap.Add (world01.ID, world01);
        universe.InnerWorldsMap.Add (world02.ID, world02);
        universe.InnerWorldsMap.Add (world03.ID, world03);
        universe.InnerWorldsMap.Add (world04.ID, world04);

        return universe;
    }
}
