﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DebugTextUI : MonoBehaviour
{

    public class MyEvent: UnityEvent<string>
    {

    }

    public static MyEvent DebugEvent = new MyEvent ();

    void OnEnable ()
    {
        DebugEvent.AddListener (ShowDebugText);
    }

    void OnDisable ()
    {
        DebugEvent.RemoveListener (ShowDebugText);
    }

    void ShowDebugText (string debugText)
    {
        GetComponent <Text> ().text += debugText + "\n";
    }
}
